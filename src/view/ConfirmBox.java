package view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmBox {
    private boolean answer;


    public boolean display(String title, String message, String valuePassed){
        Stage window = new Stage();
        window.setTitle(title);
        window.initModality(Modality.APPLICATION_MODAL);
        //window.initModality(Modality.WINDOW_MODAL);

        Label label = new Label(message);
        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");
        Label passValue = new Label(valuePassed);

        yesButton.setOnAction(e -> {
            answer = true;
            window.close();
        });

        noButton.setOnAction(e -> {
            answer = false;
            window.close();
        });

        VBox vBox = new VBox();
        vBox.getChildren().addAll(label, yesButton, noButton, passValue);
        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.showAndWait();
        return answer;
    }
}
