package view;

import constants.ButtonConstant;
import data.DummyData;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.QuestionInfo;

import java.util.function.Predicate;

public class TableViewPanel {
    private TextField searchField;
    private Button startButton, detailsView;
    private String tableName;
    private Scene scene;
    private TableView<QuestionInfo> table;
    private ObservableList<QuestionInfo> data;
    private VBox tabPanVbox;

    TableViewPanel(){

    }
    public TableViewPanel(Scene scene, String tableName){
        this.scene = scene;
        this.tableName = tableName;
        constructPanel();
    }

    private void initialize(){
        startButton = new Button("Start Test");
        detailsView = new Button("Details View");
        searchField = new TextField();
        table = new TableView();
        table.getStyleClass().add("table-view");
        searchField.setPromptText(ButtonConstant.SEARCH);
        tabPanVbox = new VBox();
    }

    private void constructPanel(){
        initialize();
        HBox hBox = new HBox();

        startButton.setOnAction(e -> {
            System.out.println(table.getSelectionModel());
            if(table.getSelectionModel().getFocusedIndex() >= 0){
                //System.out.println(table.getSelectionModel().getFocusedIndex());
                //System.out.println(table.getItems().get(table.getSelectionModel().getFocusedIndex()).getQuestionName());
                QuestionInfo qInfo = table.getItems().get(table.getSelectionModel().getFocusedIndex());
                String item = table.getItems().get(table.getSelectionModel().getFocusedIndex()).toString();
                TestWindows testWindows = new TestWindows();
                table.getSelectionModel().clearSelection();
                Scene testWindowsScene = testWindows.showTestWindows(item, qInfo);
                System.out.println(testWindowsScene);
            }
        });

        detailsView.setOnAction(e -> {
            System.out.println(table.getSelectionModel().getFocusedIndex());
            System.out.println(table.getItems().get(table.getSelectionModel().getFocusedIndex()));
            table.getItems().get(table.getSelectionModel().getFocusedIndex());
            table.getSelectionModel().clearSelection();
        });

        hBox.getChildren().addAll(startButton, searchField, detailsView);
        hBox.getStyleClass().add("hBox");
        hBox.setId("hBox-custom");

        fetchData();
        contructTablePanel();
        FilteredList<QuestionInfo> filteredList = new FilteredList<>(data, e -> true);
        searchField.setOnKeyReleased(event -> {
            searchField.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredList.setPredicate((Predicate<? super QuestionInfo>) questionInfo ->{
                    if(newValue == null && newValue.isEmpty()){
                        return true;
                    }
                    String lowerCaseFilter  = newValue.toLowerCase();
                    if(Integer.toString(questionInfo.getQuestionNumber()).contains(newValue)){
                        return true;
                    }else if(questionInfo.getQuestionName().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    }else if(questionInfo.getDifficultyLevel().toLowerCase().contains(lowerCaseFilter)){
                        return true;
                    }

                    return false;
                });
            });
            SortedList<QuestionInfo> sortedList = new SortedList<>(filteredList);
            sortedList.comparatorProperty().bind(table.comparatorProperty());
            table.setItems(sortedList);
        });

        table.getSelectionModel().clearSelection();
        tabPanVbox.getChildren().addAll(table, hBox);
        tabPanVbox.setPadding(new Insets(0, 0, 0, 5));
    }

    private void contructTablePanel() {
        TableColumn q_no = new TableColumn("Q No");
        q_no.setMinWidth(50);
        q_no.setCellValueFactory(
                new PropertyValueFactory<QuestionInfo, Integer>("questionNumber"));

        TableColumn qName = new TableColumn("Question Name");
        qName.setMinWidth(100);
        qName.setCellValueFactory(
                new PropertyValueFactory<QuestionInfo, String>("questionName"));

        TableColumn noOfQues = new TableColumn("No of Q");
        noOfQues.setMinWidth(200);
        noOfQues.setCellValueFactory(
                new PropertyValueFactory<QuestionInfo, String>("numberOfQuestion"));

        TableColumn testDuration = new TableColumn("Test Duration");
        testDuration.setMinWidth(200);
        testDuration.setCellValueFactory(
                new PropertyValueFactory<QuestionInfo, String>("testDuration"));

        TableColumn no_of_section = new TableColumn("No of Section");
        no_of_section.setMinWidth(200);
        no_of_section.setCellValueFactory(
                new PropertyValueFactory<QuestionInfo, String>("numberOfSection"));

        TableColumn level = new TableColumn("Level");
        level.setMinWidth(200);
        level.setCellValueFactory(
                new PropertyValueFactory<QuestionInfo, String>("difficultyLevel"));

        table.setItems(data);
        table.getColumns().addAll(q_no, qName, noOfQues, testDuration, no_of_section, level);
    }

    private void fetchData() {
        DummyData dummyData = new DummyData();
        data = dummyData.getData();
    }

    public VBox getTabPanVbox(){
        return tabPanVbox;
    }

}
