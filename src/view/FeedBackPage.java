package view;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.Question;
import plot.BarChartPlot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeedBackPage {

    private VBox vBox;
    private List<Question> questionList;
    private int[] attemptedList;
    private String[] texts = {"Answered", "Not Answered", "Not Visited", "Mark For Reviews", "Answered and Review"};

    public FeedBackPage(List<Question> questionList, int[] attemptedList){
        this.questionList = questionList;
        this.attemptedList = attemptedList;
        constructUI();
    }

    private void constructUI(){
        vBox = new VBox();
        CircularButton circularButton = new CircularButton(5, attemptedList, texts);

        vBox.getChildren().addAll((Node) circularButton.getUI() , getBarChartPanel());
    }

    private HBox getBarChartPanel(){
        String[] sections = {"English", "Math", "General Studies"};
        String[] levels = {"Easy", "Medium","Hard"};
        Map<String, Object> data1 = new HashMap();

        int[] list1 = {10, 12, 8};
        int[] list2 = {14, 10, 5};
        int[] list3 = {8, 15, 7};
        data1.put(sections[0], list1);
        data1.put(sections[1], list2);
        data1.put(sections[2], list3);

        Map<String, Object> data11 = new HashMap();
        int[] list11 = {8, 6, 3};
        int[] list21 = {10, 7, 2};
        int[] list31 = {8, 10, 3};
        data11.put(sections[0], list11);
        data11.put(sections[1], list21);
        data11.put(sections[2], list31);


        BarChartPlot barChartPlotleft = new BarChartPlot(sections, levels, data1);
        BarChartPlot barChartPlotright = new BarChartPlot(sections, levels, data11);
        HBox hBox = new HBox();
        hBox.getChildren().addAll(barChartPlotleft.getBarChart(), barChartPlotright.getBarChart());
        hBox.setSpacing(10);
        return hBox;
    }

    public VBox getvBox() {
        return vBox;
    }
}

