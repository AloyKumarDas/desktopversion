package view;

import constants.ColorConstant;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.QuestionInfo;

import java.util.ArrayList;
import java.util.List;

public class MockTestRightPanel {
    private FlowPane flowPane;
    private Stage stage;
    private Scene scene, feedBackScene;
    private Pagination pagination;
    // Variable declare for timer
    private Integer minute;
    private static final Integer STARTTIME = 00;
    private Timeline timeline;
    private Label timerLabel = new Label();
    private Integer timeSeconds = STARTTIME;
    private ProgressIndicator progressIndicator = new ProgressIndicator();
    private float inrValue;
    private Scene oldScene, newScene;
    private VBox vBox;
    private LegendAndStats legendAndStats;
    private List<Button> buttonList = new ArrayList<>();
    private QuestionSet questionSet;
    private QuestionInfo questionInfo;

    public MockTestRightPanel(Pagination pagination, QuestionInfo questionInfo, QuestionSet questionSet) {
        this.pagination = pagination;
        this.questionInfo = questionInfo;
        this.questionSet = questionSet;
        constructMockTestPane();
    }

    public void setOldScene(Scene oldScene) {
        this.oldScene = oldScene;
    }

    public void setNewScene(Scene newScene) {
        this.newScene = newScene;
    }

    public MockTestRightPanel(Stage stage, Scene scene){
        this.stage = stage;
        this.scene = scene;
        constructMockTestPane();
    }

    private void constructMockTestPane() {
        minute = questionInfo.getTestDuration();
        inrValue = 1f/(minute*60f);

        vBox = new VBox();
        vBox.setMaxWidth(200);

        // Configure the Label
        progressIndicator.setProgress(0);
        //progressIndicator.setMinHeight(30);
        //progressIndicator.setMaxWidth(30);
        progressIndicator.setPadding(new Insets(0,30, 0,30));

        timerLabel.setText(minute.toString() +" : "+ timeSeconds.toString());
        timerLabel.setTextFill(Color.GREEN);
        timerLabel.setPadding(new Insets(0,10, 0,20));
        timerLabel.setStyle("-fx-font-size: 3em;");


        // Flow pane code
        flowPane = new FlowPane(Orientation.HORIZONTAL, 5, 5);
        flowPane.setPadding(new Insets(5,5,5,5));
        flowPane.setMaxWidth(200);
        for (int i = 1; i <= questionInfo.getNumberOfQuestion(); i++) {
            Button tmp = getButton(i);
            buttonList.add(tmp);
            flowPane.getChildren().add(tmp);
        }
        flowPane.setStyle("-fx-background-color:white;");


        DropShadow ds = new DropShadow();
        ds.setOffsetY(5.0);
        ds.setOffsetX(5.0);
        ds.setColor(Color.GRAY);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setStyle("-fx-background-color:white;");
        scrollPane.setEffect(ds);
        scrollPane.fitToHeightProperty().set(true);
        scrollPane.setContent(flowPane);
        scrollPane.setMinHeight(200);
        scrollPane.setMinWidth(100);
        scrollPane.setMaxWidth(200);
        scrollPane.setPrefSize(200, 200);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        // Question Marking button
        legendAndStats = new LegendAndStats();

        HBox timerText = new HBox();
        timerText.getChildren().add(timerLabel);
        timerText.setAlignment(Pos.CENTER);


        vBox.setSpacing(20);
        vBox.setPadding(new Insets(10,20, 10, 10));
        vBox.getChildren().addAll(progressIndicator, timerText, scrollPane, legendAndStats.getUI());
    }


    public void startTimer(){

        if(timeline != null){
            timeline.stop();
        }
        timeSeconds = STARTTIME;
        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        //System.out.println("Before "+ timeSeconds);
                        timeSeconds--;
                        if(timeSeconds < 0){
                            minute--;
                            timeSeconds = 59;
                        }
                        // Change color
                        if(minute == 0 && timeSeconds < 30){
                            timerLabel.setTextFill(Color.RED);
                        }
                        // Change color
                        progressIndicator.setProgress(progressIndicator.getProgress()+ inrValue);
                        //System.out.println("After "+ inrValue );
                        timerLabel.setText(minute.toString() + " : "+timeSeconds.toString());
                        if(minute == 0 && timeSeconds <=0){
                            returnMethod(timeline);
                            timeline.stop();
                        }
                        if(minute < 0 || timeSeconds < 0 ) {
                            returnMethod(timeline);
                            timeline.stop();
                        }
                    }
                })
        );
        timeline.playFromStart();
    }

    private void returnMethod(Timeline timeline){
        timeline.stop();
        questionSet.submitTest();
        //((Stage)newScene.getWindow()).setScene(feedBackScene);
    }

    private Button getButton(Object index){
        Button bt = new Button(index.toString());
        bt.setOnAction(e -> clickAction(bt));
        bt.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent t) {
                String[] styleList = bt.getStyle().split(";");
                if(styleList.length == 5){
                    bt.setTooltip(new Tooltip("Un Atempted."));
                }else {
                    bt.setTooltip(new Tooltip("Attempted."));
                }
            }
        });
        bt.setShape(new Circle(1.2));
        bt.setMaxSize(3,3);
        bt.setStyle(ColorConstant.NOT_VISITED);
        return bt;
    }

    public void setButtonColor(int index, String color){
        buttonList.get(index).setStyle(color);
    }

    private void clickAction(Button bt) {
        pagination.setCurrentPageIndex(Integer.parseInt(bt.getText()) - 1);
        bt.setStyle(ColorConstant.ACTIVE_QUESTION);
    }

    public VBox getPanel(){
        return vBox;
    }

    public void setLegendPanelValue(int[] value) {
        legendAndStats.setValues(value);
    }

    public void changeNumberInLegend(){
        //.getBackground.BACKGROUND_COLOR.property.value
        System.out.println(flowPane.getChildren().get(0).getStyleClass().get(0));
    }
}
