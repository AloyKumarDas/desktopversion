package view;

import constants.ColorConstant;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.util.List;

public class CircularButton {

    private int number;
    private int[] valueOnCircle;
    private String[] texts;
    private String orientation = "HORIZONTAL";
    private HBox hBox;
    private VBox vBox;

    public CircularButton(int number, int[] valueOnCircle, String[] texts){
        this.number = number;
        this.valueOnCircle= valueOnCircle;
        this.texts = texts;
        constructUI();
    }

    private void constructUI(){
        hBox = new HBox();
        vBox = new VBox();
        for(int i = 0; i <  valueOnCircle.length; i++){
            Circle circle = getCircle(ColorConstant.COLOR_LIST[i]);
            Text text = getText(texts[i]);
            StackPane stackPane = new StackPane();
            stackPane.getChildren().addAll(circle, new Text(Integer.toString(valueOnCircle[i])));
            if(orientation.equals("HORIZONTAL")){
                VBox temp = new VBox();
                temp.getChildren().addAll(stackPane, text);
                temp.setSpacing(5);
                hBox.getChildren().add(temp);

                hBox.getStyleClass().add("hBox");
                hBox.setId("hBox-custom");
            }else {
                HBox temp = new HBox();
                temp.getChildren().addAll(stackPane, text);
                temp.setSpacing(5);
                vBox.getChildren().add(temp);
            }
        }
    }

    private Text getText(String value) {
        Text text = new Text();
        //t.setEffect(ds);
        text.setCache(true);
        text.setX(20.0f);
        text.setY(70.0f);
        text.setFill(Color.BLACK);
        text.setText(value);
        text.setFont(Font.font("null", FontWeight.NORMAL, 14));
        return text;
    }
    private Circle getCircle(String color){
        Circle circle = new Circle();
        //circle.setEffect(ds1);
        circle.setCenterX(20.0f);
        circle.setCenterY(325.0f);
        circle.setRadius(15.0f);
        circle.setFill(Color.web(color));
        circle.setCache(true);
        return circle;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public Object getUI(){
        if(orientation.equals("HORIZONTAL")) {
            return hBox;
        }
        return vBox;
    }

}
