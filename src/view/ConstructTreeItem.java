package view;

import constants.ButtonConstant;
import constants.Constant;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import reader.JSONReaderWriter;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

public class ConstructTreeItem {

    private TreeItem rootItem;
    private JSONObject jsonObject;
    private String sideBarButtonText;

    public ConstructTreeItem(String sideBarButtonText){
        this.sideBarButtonText = sideBarButtonText;
        rootItem = new TreeItem(sideBarButtonText);
        fetchTreeItem();
    }

    private void fetchTreeItem(){

        switch (sideBarButtonText){
            case ButtonConstant.PRACTICE_SETS:
                constructTreeItems(ButtonConstant.PRACTICE_SETS);
                break;

            case ButtonConstant.ONLINE_SETS :
                constructTreeItems(ButtonConstant.ONLINE_SETS);
                break;

            case ButtonConstant.USER_SETS:
                constructTreeItems(ButtonConstant.USER_SETS);
                break;

            default:
                System.out.println("Unknown choice");
                break;
        }
    }

    private TreeItem constructTreeItems(String sideBarButtonText){
        //rootItem = new TreeItem(sideBarButtonText);
        constructMapFromJSONFile(sideBarButtonText);
        buildTree("All", "All");
        return rootItem;
    }

    public JSONObject constructMapFromJSONFile(String sideBarButtonText){
        jsonObject = null;
        if(sideBarButtonText.toLowerCase().contains(Constant.PRACTICE))
            jsonObject = JSONReaderWriter.getJsonObject(Constant.PRACTICE);
        else if(sideBarButtonText.toLowerCase().contains(Constant.ONLINE))
            jsonObject = JSONReaderWriter.getJsonObject(Constant.ONLINE);
        else if(sideBarButtonText.toLowerCase().contains(Constant.USER))
            jsonObject = JSONReaderWriter.getJsonObject(Constant.USER);

        return jsonObject;
    }

    public TreeItem getTreeItem() {
        return rootItem;
    }

    public void updateTreeItem(TreeView treeView, String subject, String subTopic){
        buildTree(subject, subTopic);
        treeView.setRoot(rootItem);
    }

    private void buildTree(String subjectName, String topic){
        Map<String, JSONArray> sets = new HashMap();
        rootItem.getChildren().clear();
        if(!subjectName.equals(Constant.ALL)){
            sets.putAll((HashMap) jsonObject.get(subjectName));
        }else {
            sets.putAll((HashMap) jsonObject.get(Constant.MATH));
            sets.putAll((HashMap) jsonObject.get(Constant.ENGLISH));
        }
        for (Map.Entry<String, JSONArray> entry : sets.entrySet()){
            try {
                FileInputStream fileInputStream = new FileInputStream("C:\\Users\\USER\\IdeaProjects\\ShopManagement\\src\\images\\icons8-folder-20.png");
                Image cameraIcon = new Image(fileInputStream);
                ImageView cameraIconView = new ImageView(cameraIcon);
                TreeItem treeItem = new TreeItem(entry.getKey(), cameraIconView);
                JSONArray values = entry.getValue();
                for (int i = 0; i < values.size(); i++) {
                    FileInputStream fileInputStream1 = new FileInputStream("C:\\Users\\USER\\IdeaProjects\\ShopManagement\\src\\images\\icons8-file-20.png");
                    Image cameraIcon1 = new Image(fileInputStream1);
                    ImageView cameraIconView1 = new ImageView(cameraIcon1);

                    TreeItem treeItem1 = new TreeItem(values.get(i), cameraIconView1);
                    treeItem.getChildren().add(treeItem1);
                }
                rootItem.getChildren().add(treeItem);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


}
