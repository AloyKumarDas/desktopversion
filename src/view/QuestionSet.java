package view;

import constants.ColorConstant;
import constants.QuestionConstant;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Question;
import model.QuestionInfo;
import utils.QuestionUtils;
import java.util.List;

public class QuestionSet {

    private List<Question> questionsList;
    private Pagination pagination;
    private HBox hBoxTemp;
    private MockTestRightPanel mockTestRightPanel;
    private Button marks_for_review, submit, save_and_next, clearResponse;
    private Scene newScene, oldScene;
    private int currentIndex = 0;
    private String clickButton = "";
    private int[] attemtedList = {0,0,0,0,0};
    private String[] sections = {"English", "Mathematics", "Reasoning"};
    private Node[] pageList;
    private boolean isAnswered = false;

    private QuestionInfo questionInfo;


    public QuestionSet(QuestionInfo questionInfo){
        this.questionInfo = questionInfo;
    }

    public QuestionSet(Scene scene){
        this.oldScene = scene;
    }

    public Scene init() {
        VBox rightPanel = new VBox();
        questionsList = QuestionUtils.getQuestionSets(questionInfo);
        attemtedList[2] = questionsList.size();
        pageList = new Node[questionsList.size()];
        // Pagination structure
        pagination = new Pagination(questionsList.size(), 0);

        //pagination.setStyle("-fx-border-color:red;");
        //pagination.setStyle("-fx-arrows-visible: false");
        //pagination.getStyleClass().add(Pagination.STYLE_CLASS_BULLET);

        pagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {

                if (clickButton.equals(QuestionConstant.SAVE_AND_NEXT)) {
                    mockTestRightPanel.setButtonColor(currentIndex, ColorConstant.ANSWERED);
                    isAnswered = false;
                }else if(clickButton.equals(QuestionConstant.MARK_AND_REVIEW)){
                    if(isAnswered){
                        mockTestRightPanel.setButtonColor(currentIndex, ColorConstant.ANSWERED_AND_REVIEW);
                        isAnswered = false;
                    }else
                        mockTestRightPanel.setButtonColor(currentIndex, ColorConstant.MARK_FOR_REVIEW);
                }else{
                    Question tmp = questionsList.get(currentIndex);
                    String status = tmp.getStatus();
                    if(status != null && status.equals(QuestionConstant.MARK_AND_REVIEW))
                        if(isAnswered || tmp.isAnswer()){
                            mockTestRightPanel.setButtonColor(currentIndex, ColorConstant.ANSWERED_AND_REVIEW);
                            isAnswered = false;
                        }else
                            mockTestRightPanel.setButtonColor(currentIndex, ColorConstant.MARK_FOR_REVIEW);
                    else if(status != null && status.equals(QuestionConstant.SAVE_AND_NEXT)){
                        mockTestRightPanel.setButtonColor(currentIndex, ColorConstant.ANSWERED);
                    }else {
                        mockTestRightPanel.setButtonColor(currentIndex, ColorConstant.NOT_ANSWERED);
                    }
                }
                String currentPageStatus = questionsList.get(pageIndex).getStatus();
                if(currentPageStatus == null) {
                    questionsList.get(pageIndex).setStatus(QuestionConstant.NOT_ANSWERED);
                    attemtedList[1] += 1;
                    attemtedList[2] -= 1;
                }

                if(currentIndex != pageIndex) {
                    currentIndex = pageIndex;
                }

                Node node = createPage(pageIndex);
                pageList[pageIndex] = node;

                mockTestRightPanel.setButtonColor(pageIndex, ColorConstant.ACTIVE_QUESTION);
                mockTestRightPanel.setLegendPanelValue(attemtedList);
                clickButton = "";
                return node;
            }
        });

        mockTestRightPanel = new MockTestRightPanel(pagination, questionInfo, this);
        mockTestRightPanel.setButtonColor(0, ColorConstant.ACTIVE_QUESTION);
        mockTestRightPanel.setLegendPanelValue(attemtedList);
        rightPanel.getChildren().add(mockTestRightPanel.getPanel());

        AnchorPane anchor = new AnchorPane();
        anchor.setMinHeight(500);
        anchor.setMaxHeight(Double.MAX_VALUE);
        AnchorPane.setTopAnchor(pagination, 10.0);
        AnchorPane.setRightAnchor(pagination, 10.0);
        AnchorPane.setBottomAnchor(pagination, 10.0);
        AnchorPane.setLeftAnchor(pagination, 10.0);
        anchor.getChildren().addAll(pagination);

        marks_for_review = new Button(QuestionConstant.MARK_AND_REVIEW);
        marks_for_review.setOnAction(e -> saveValue(marks_for_review));

        submit = new Button(QuestionConstant.SUBMIT);
        submit.setOnAction(e -> submitTest());

        clearResponse = new Button(QuestionConstant.CLEAR_RESPONSE);
        clearResponse.setOnAction(e -> saveValue(clearResponse));

        save_and_next = new Button(QuestionConstant.SAVE_AND_NEXT);
        save_and_next.setOnAction(e -> saveValue(save_and_next));

        HBox leftBox = new HBox(clearResponse);
        leftBox.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(leftBox, Priority.ALWAYS);

        HBox centerBox = new HBox(marks_for_review);
        centerBox.setAlignment(Pos.CENTER);
        HBox.setHgrow(centerBox, Priority.ALWAYS);

        HBox rightBox = new HBox(save_and_next);
        rightBox.setAlignment(Pos.CENTER_RIGHT);
        HBox.setHgrow(rightBox, Priority.ALWAYS);

        HBox bottom = new HBox(leftBox, centerBox, rightBox);
        bottom.setPadding(new Insets(10));

        HBox submitBox = new HBox(submit);
        submitBox.setAlignment(Pos.CENTER);
        HBox.setHgrow(submitBox, Priority.ALWAYS);

        HBox saveBottom = new HBox(submitBox);
        saveBottom.setPadding(new Insets(10));


        Label heading = new Label(questionInfo.getQuestionName());
        heading.setFont(Font.font(20));

        VBox vBox = new VBox();
        vBox.getChildren().addAll(getSectionPanel(sections), anchor, bottom, saveBottom);
        vBox.setSpacing(5);

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(heading);
        borderPane.setRight(rightPanel);
        borderPane.setCenter(vBox);
        newScene = new Scene(borderPane, 900, 650);

        return newScene;
    }

    public void submitTest() {
        FeedBackPage feedBackPage = new FeedBackPage(questionsList, attemtedList);
        VBox tmp = ((VBox)oldScene.getRoot());
        tmp.getChildren().clear();
        tmp.getChildren().add(feedBackPage.getvBox());
        try {
            ((Stage) newScene.getWindow()).setScene(oldScene);
        }catch (NullPointerException n){
            System.out.println(n.getMessage());
        }
    }

    public void startTimeMethod(){
        mockTestRightPanel.setOldScene(oldScene);
        mockTestRightPanel.setNewScene(newScene);
        mockTestRightPanel.startTimer();
    }

    private void saveValue(Button button){
        Question question = questionsList.get(pagination.getCurrentPageIndex());
        Object[] value = new Object[hBoxTemp.getChildren().size()];

        if(question.getQuestionType().equals("M")){
            for(int i = 0; i < hBoxTemp.getChildren().size(); i++) {
                boolean chkValue = ((CheckBox) hBoxTemp.getChildren().get(i)).isSelected();
                if(chkValue) {
                    isAnswered = chkValue;
                    question.setAnswer(true);
                }
                value[i] = chkValue;
            }
        }else if(question.getQuestionType().equals("S")){
            for(int i = 0; i < hBoxTemp.getChildren().size(); i++) {
                boolean chkValue = ((RadioButton) hBoxTemp.getChildren().get(i)).isSelected();
                if(chkValue){
                    isAnswered = chkValue;
                    question.setAnswer(true);
                }
                value[i] = chkValue;
            }
        }else if(question.getQuestionType().equals("T")){
            value[0] = ((TextField)hBoxTemp.getChildren().get(0)).getText();
            if(!value[0].equals("")){
                isAnswered = true;
                question.setAnswer(true);
            }
        }
        question.setSolution(value);
        String status = question.getStatus();

        if(button.getText().equals(QuestionConstant.CLEAR_RESPONSE)){
            resetSolution(pagination.getCurrentPageIndex());
            clickButton = QuestionConstant.CLEAR_RESPONSE;
            mockTestRightPanel.setButtonColor(pagination.getCurrentPageIndex() , ColorConstant.ACTIVE_QUESTION);

        }else if(button.getText().equals(QuestionConstant.SAVE_AND_NEXT)){
            clickButton = QuestionConstant.SAVE_AND_NEXT;
            mockTestRightPanel.setButtonColor(pagination.getCurrentPageIndex() , ColorConstant.ANSWERED);
            pagination.setCurrentPageIndex(pagination.getCurrentPageIndex() + 1);
            mockTestRightPanel.setButtonColor(pagination.getCurrentPageIndex(), ColorConstant.ACTIVE_QUESTION);
            if(status != null && status.equals(QuestionConstant.MARK_AND_REVIEW)){
                if(isAnswered)
                    attemtedList[4] -= 1;
                else
                    attemtedList[3] -= 1;
                attemtedList[0] += 1;
            }else if (status != null && status.equals(QuestionConstant.NOT_ANSWERED)){
                attemtedList[0] += 1;
                attemtedList[1] -= 1;
            }
            mockTestRightPanel.setLegendPanelValue(attemtedList);
            question.setStatus(QuestionConstant.SAVE_AND_NEXT);

        }else if(button.getText().equals(QuestionConstant.MARK_AND_REVIEW)){
            clickButton = QuestionConstant.MARK_AND_REVIEW;
            String color = null;
            if(status != null && status.equals(QuestionConstant.SAVE_AND_NEXT) && isAnswered){
                attemtedList[4] += 1;
                attemtedList[0] -= 1;
            }else if (status != null && status.equals(QuestionConstant.NOT_ANSWERED)){
                if(isAnswered) {
                    attemtedList[4] += 1;
                    color = ColorConstant.ANSWERED_AND_REVIEW;
                    question.setAnswer(true);
                }
                else {
                    attemtedList[3] += 1;
                    color = ColorConstant.MARK_FOR_REVIEW;
                }
                attemtedList[1] -= 1;
            }
            question.setStatus(QuestionConstant.MARK_AND_REVIEW);
            mockTestRightPanel.setButtonColor(pagination.getCurrentPageIndex() , color);
            pagination.setCurrentPageIndex(pagination.getCurrentPageIndex() + 1);

            mockTestRightPanel.setButtonColor(pagination.getCurrentPageIndex(), ColorConstant.ACTIVE_QUESTION);
            mockTestRightPanel.setLegendPanelValue(attemtedList);
        }
    }

    private VBox createPage(int pageIndex) {

        VBox box = new VBox(5);
        Question qstn = questionsList.get(pageIndex);
        int pageNumber = pageIndex ;
        Label qs = new Label(qstn.getQuestion());
        Label qsNo = new Label("Question : "+ pageNumber + 1);
        qs.setWrapText(true);
        qs.setFont(Font.font(20));
        qsNo.setFont(Font.font(20));

        String[] option = qstn.getOptions();
        Object[] solution = qstn.getSolution();
        hBoxTemp = new HBox();

        if(qstn.getQuestionType().equals("M")) {
            for (int op = 0; op < option.length; op++) {
                CheckBox checkBox = new CheckBox(option[op]);

                if(solution != null)
                    checkBox.setSelected((boolean)solution[op]);

                checkBox.setMinSize(5, 5);
                hBoxTemp.getChildren().add(checkBox);
            }
        }else if(qstn.getQuestionType().equals("S")){
            for (int op = 0; op < option.length; op++) {
                RadioButton radioButton = new RadioButton(option[op]);

                if(solution != null)
                    radioButton.setSelected((boolean)solution[op]);

                radioButton.setMinSize(5, 5);
                hBoxTemp.getChildren().addAll(radioButton);
            }
        }else if(qstn.getQuestionType().equals("T")){
            TextField textField = new TextField();

            if(solution != null)
                textField.setText(solution[0].toString());

            textField.setPromptText("Enter Answer");
            hBoxTemp.getChildren().addAll(textField);
        }

        hBoxTemp.setPadding(new Insets(1,5,1, 5));
        hBoxTemp.setSpacing(30);
        box.getChildren().addAll(qsNo, qs, hBoxTemp);
        box.setSpacing(10);
        box.setPadding(new Insets(10, 10, 10, 10));

        return box;
    }

    private HBox getSectionPanel(String[] sectionList){
        HBox hBox = new HBox();
        for(int i = 0; i < sectionList.length; i++){
            Button button = new Button(sectionList[i]);
            button.setMaxWidth(100);
            button.setMaxHeight(10);
            button.setDisable(true);
            hBox.getChildren().add(button);
        }
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(0, 1,1,20));
        return hBox;
    }

    private void resetSolution(int index){
        Node node = pageList[index];
        HBox hBox = (HBox)(((VBox) node).getChildren().get(2));

        questionsList.get(index).setAnswer(false);
        questionsList.get(index).setStatus(null);

        Object[] value = hBox.getChildren().toArray();

        if(value[0] instanceof RadioButton){
            for(int i = 0; i < value.length; i++) {
                ((RadioButton) value[i]).setSelected(false);
            }
        }else if(value[0] instanceof CheckBox){
            for(int i = 0; i < value.length; i++) {
                ((CheckBox) value[i]).setSelected(false);
            }
        }else if(value[0] instanceof TextField){
            ((TextField) value[0]).setText("");
        }
    }

    public void setOldScene(Scene oldScene) {
        this.oldScene = oldScene;
    }

}
