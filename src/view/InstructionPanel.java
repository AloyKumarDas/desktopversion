package view;

import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.QuestionInfo;
import utils.QuestionUtils;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class InstructionPanel implements Initializable {
    private VBox vBox, outerPanel;
    private Scene scene, newScene;
    private QuestionSet questionSet;
    private QuestionInfo questionInfo;

    public InstructionPanel(Scene scene, QuestionInfo questionInfo){
        this.scene = scene;
        this.questionInfo = questionInfo;
    }

    private void initialize(){

        questionSet = new QuestionSet(questionInfo);
        newScene = questionSet.init();

        vBox = new VBox();
        outerPanel = new VBox();

        Label instruction = new Label("Instruction");
        instruction.setFont(Font.font(20));

        ScrollPane scrollPane = new ScrollPane();
        //List<String> textList = getTextList();
        List<String> textList = QuestionUtils.getInfoText(questionInfo);
        for(int i = 0;i < textList.size(); i++){
            HBox hBox = new HBox();
            Label index = new Label(Integer.toString(i+1)+".");
            Label info = new Label(textList.get(i));
            hBox.getChildren().addAll(index, info);
            hBox.setSpacing(10);
            vBox.getChildren().add(hBox);
        }
        vBox.setSpacing(10);
        scrollPane.setContent(vBox);

        CheckBox checkBox = new CheckBox("I have read and understood the instructions.");
        Button start = new Button("Start");
        HBox hBox = new HBox(start);
        hBox.setAlignment(Pos.CENTER);
        checkBox.setOnAction(e->{
            start.setDisable(!checkBox.isSelected());
        });
        start.setDisable(true);
        start.getStyleClass().add("#font-button");

        start.setOnAction(e-> changeScene());

        outerPanel.getChildren().addAll(instruction, scrollPane, checkBox, hBox);
        outerPanel.setSpacing(20);
        outerPanel.setPadding(new Insets(100,50, 100,50));

    }

    private void changeScene() {
        System.out.println("Scene change");
        questionSet.setOldScene(scene);
        questionSet.startTimeMethod();
        questionSet.setOldScene(scene);

        ((Stage)scene.getWindow()).setScene(newScene);
        ((Stage)newScene.getWindow()).setMaxHeight(Double.MAX_VALUE);
        ((Stage)newScene.getWindow()).setMaxWidth(Double.MAX_VALUE);
    }

    public VBox getInstructionPanel(){
        initialize();
        return outerPanel;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
