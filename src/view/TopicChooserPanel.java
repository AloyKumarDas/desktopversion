package view;

import constants.Constant;
import constants.TextConstant;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import sample.LeftPanel;

public class TopicChooserPanel {
    private Scene scene;
    private LeftPanel leftPanel;
    private ComboBox<String> subjectsSubsetComboBox;
    private ComboBox<String> subjectsListComboBox;

    TopicChooserPanel(){
        subjectsListComboBox = new ComboBox<>();
        subjectsListComboBox.setPromptText(TextConstant.TOPIC);
        subjectsListComboBox.getItems().addAll(Constant.subjects);
        subjectsListComboBox.setMinWidth(150);
        subjectsListComboBox.setMaxWidth(Double.MAX_VALUE);

        subjectsSubsetComboBox = new ComboBox<>();
        subjectsSubsetComboBox.setPromptText(TextConstant.SUB_TOPIC);
        subjectsSubsetComboBox.setMinWidth(150);
        subjectsSubsetComboBox.setMaxWidth(Double.MAX_VALUE);

    }

    TopicChooserPanel(Scene scene, LeftPanel leftPanel){
        this.scene = scene;
        this.leftPanel = leftPanel;

    }

    private void initialize(){

    }

    private void constructPanel(){

    }


    public ComboBox<String> getSubjectsSubsetComboBox() {
        return subjectsSubsetComboBox;
    }

    public void setSubjectsSubsetComboBox(ComboBox<String> subjectsSubsetComboBox) {
        this.subjectsSubsetComboBox = subjectsSubsetComboBox;
    }

    public ComboBox<String> getSubjectsListComboBox() {
        return subjectsListComboBox;
    }

    public void setSubjectsListComboBox(ComboBox<String> subjectsListComboBox) {
        this.subjectsListComboBox = subjectsListComboBox;
    }
}
