package view;

import constants.ColorConstant;
import javafx.geometry.Insets;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;


public class LegendAndStats {

    private Text answered, notAnswered, markForReview, notVisited, answeredAndReview;
    private Text[] textList = {answered, notAnswered, notVisited, markForReview, answeredAndReview};
    private String[] texts = {"Answered", "Not Answered", "Not Visited", "Mark For Reviews", "Answered and Review"};

    private int[] values = {0,0,0,0,0};
    private VBox legendsPanel;

    public void LegendAndStats(){
    }

    public void LegendAndStats(int[] values){
        this.values = values;
    }

    private void initialize(){
        legendsPanel = new VBox();
        for (int i = 0 ; i < values.length; i++){
            textList[i] = new Text(Integer.toString(values[i]));
        }
    }


    private VBox getList(){
        initialize();
        VBox vBox = new VBox();

        DropShadow ds = new DropShadow();
        ds.setOffsetY(5.0);
        ds.setOffsetX(5.0);
        ds.setColor(Color.GRAY);

        for (int i = 0 ;i < values.length; i++){
            Circle circle = getCircle(ColorConstant.COLOR_LIST[i]);
            Text text = getText(texts[i]);
            StackPane stackPane = new StackPane();
            stackPane.getChildren().addAll(circle, textList[i]);
            HBox hBox = new HBox();
            hBox.getChildren().addAll(stackPane, text);
            hBox.setSpacing(10);

            vBox.getChildren().add(hBox);
        }
        vBox.setSpacing(10);
        vBox.setEffect(ds);
        vBox.setPadding(new Insets(10,5,10,5));
        vBox.setMinHeight(220);
        vBox.setMinWidth(200);
        vBox.setStyle("-fx-background-color: white;" + "-fx-border-radius: 0 0 18 18;");
        return vBox;
    }

    private Text getText(String value) {
        Text text = new Text();
        //t.setEffect(ds);
        text.setCache(true);
        text.setX(20.0f);
        text.setY(70.0f);
        text.setFill(Color.BLACK);
        text.setText(value);
        text.setFont(Font.font("null", FontWeight.NORMAL, 14));
        return text;
    }
    private Circle getCircle(String color){
        Circle circle = new Circle();
        //circle.setEffect(ds1);
        circle.setCenterX(20.0f);
        circle.setCenterY(325.0f);
        circle.setRadius(15.0f);
        circle.setFill(Color.web(color));
        circle.setCache(true);
        return circle;
    }

    public void setValues(int[] values){
        for (int i = 0; i < values.length; i++ ){
            textList[i].setText(Integer.toString(values[i]));
        }
    }

    public VBox getUI(){
        return getList();
    }

}
