package view;

import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.QuestionInfo;

public class TestWindows {
    private Scene oldScene;
    private Stage testWindows;

    public Scene showTestWindows(String title, QuestionInfo questionInfo){
        testWindows = new Stage();
        testWindows.setTitle(title);
        testWindows.initModality(Modality.APPLICATION_MODAL);

        VBox vBox = new VBox();

        oldScene = new Scene(vBox, 900, 650);
        String styleSheet = getClass().getResource("testWindowsStyle.css").toExternalForm();
        oldScene.getStylesheets().add(styleSheet);

        InstructionPanel instructionPanel = new InstructionPanel(oldScene, questionInfo);
        vBox.getChildren().addAll(instructionPanel.getInstructionPanel());

        testWindows.setScene(oldScene);
        testWindows.showAndWait();
        return testWindows.getScene();
    }
}
