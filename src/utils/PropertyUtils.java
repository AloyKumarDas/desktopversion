package utils;

import constants.DBConstant;
import java.io.FileReader;
import java.util.Properties;
import java.util.logging.Logger;

public class PropertyUtils {

    private static Logger logger = Logger.getLogger(PropertyUtils.class.getName());

    public static Properties getProperties(){
        try {
            FileReader fileReader = new FileReader(DBConstant.INFO_FILE);
            Properties properties = new Properties();
            properties.load(fileReader);
            return properties;
        }catch (Exception e){
            logger.info(e.getMessage());
            return null;
        }
    }
}
