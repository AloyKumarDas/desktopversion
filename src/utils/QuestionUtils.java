package utils;

import javafx.scene.text.Text;
import model.Question;
import model.QuestionInfo;
import model.QuestionList;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionUtils {

    public static Map<String, Object> constructDataForPlot(QuestionList questionList){
        Map<String, Object> data = new HashMap<>();


        return data;
    }


    public static List<Question> getQuestionSets(QuestionInfo questionInfo){
        List<Question> questionsList = new ArrayList<>();
        String csvFile = "C:\\Users\\USER\\Desktop\\javafx\\demoData\\demo.csv";
        String line = "";
        String cvsSplitBy = ",";
        int num = questionInfo.getNumberOfQuestion();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
                num--;
                String[] qlist = line.split(cvsSplitBy);
                if(qlist[qlist.length - 1].trim().equals("M")){
                    String[] option = {qlist[1], qlist[2], qlist[3], qlist[4]};
                    questionsList.add(new Question(qlist[0], option,"M",""));
                }else if(qlist[qlist.length - 1].trim().equals("S")){
                    String[] option = {qlist[1], qlist[2], qlist[3], qlist[4]};
                    questionsList.add(new Question(qlist[0], option,"S",""));
                }else if(qlist[qlist.length - 1].trim().equals("T")){
                    String[] option = {"Text"};
                    questionsList.add(new Question(qlist[0], option,"T",""));
                }
                if(num == 0){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return questionsList;
    }


    public static List<String> getInfoText(QuestionInfo questionInfo){
        List<String> infoList = new ArrayList<>();
        infoList.add("This is a FREE online test. DO NOT pay money to anyone to attend this test.");
        infoList.add("Total duration of this test is "+ questionInfo.getTestDuration() + " minutes.");
        infoList.add("There will be " + questionInfo.getNumberOfQuestion() + " MCQ questions.");
        infoList.add("Each question carry 1 mark, no negative marks.");
        infoList.add("All the best :-).");
        return infoList;
    }
}
