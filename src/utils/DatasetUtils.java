package utils;

import data.Database;
import model.Question;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class DatasetUtils {

    private static Logger logger = Logger.getLogger(DatasetUtils.class.getName());

    public static void basicDBSetup(String dbName){
        logger.info("Creating database...");
        Database database = new Database(dbName);
        database.createDatabase();
        logger.info("Database created.");
    }


    public static String getSetTableName(){

        return null;
    }

    public static List<Object> readTable(String tableId){

        return null;
    }

    public static List<Question> readBooksFromCSV(String fileName) {
        List<Question> questionsList = new ArrayList<>();
        String csvFile = "C:\\Users\\USER\\Desktop\\javafx\\demoData\\demo.csv";
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] qlist = line.split(cvsSplitBy);
                if(qlist[qlist.length - 1].trim().equals("M")){
                    String[] option = {qlist[1], qlist[2], qlist[3], qlist[4]};
                    questionsList.add(new Question(qlist[0], option,"M",""));
                }else if(qlist[qlist.length - 1].trim().equals("S")){
                    String[] option = {qlist[1], qlist[2], qlist[3], qlist[4]};
                    questionsList.add(new Question(qlist[0], option,"S",""));
                }else if(qlist[qlist.length - 1].trim().equals("T")){
                    String[] option = {"Text"};
                    questionsList.add(new Question(qlist[0], option,"T",""));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return questionsList;
    }

    public static void updateTableValue(String tableId, String value){

    }
}
