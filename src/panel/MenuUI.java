package panel;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import media.IconLoader;
import model.Question;
import parser.IParser;
import parser.ParseUtils;

import java.io.*;
import java.util.List;

public class MenuUI {

    private String menuName;
    private Stage stage, parent;
    private Scene scene;
    private TableView<Question> table;
    private VBox vBox;
    private File file = null;
    private ObservableList<Question> data;

    public MenuUI(Stage parent, String menuName){
        this.menuName = menuName;
        this.parent = parent;
    }

    private void initialize(){
        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.getIcons().add(IconLoader.getToolIcon());

        vBox = new VBox();
        scene = new Scene(vBox, 1000, 600);
        stage.setTitle("Create new sets");
        table = new TableView<>();
        table.setVisible(false);
        contructTablePanel();
    }

    public void constructUI(){
        initialize();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Files", "*.*"));

        Label heading = new Label("This Question sets will be added in user sets");

        Label label = new Label("Choose file ");
        Button browse = new Button("Browse");

        HBox hBox = new HBox();
        hBox.getChildren().addAll(label, browse);
        hBox.setSpacing(20);

        Button loadData = new Button("Load data");

        Button uploadData = new Button("upload data");

        HBox hBox1 = new HBox();
        hBox1.getChildren().addAll(loadData, uploadData);
        hBox1.setSpacing(20);

        browse.setOnAction(e -> {
            try {
                file = fileChooser.showOpenDialog(stage);
                //InputStream stream = new FileInputStream(file);
                //Image image = new Image(stream);
                browse.setText(file.getName());
                System.out.println("File Chosen :- " + file);
            }
            catch (Exception exp) {
                exp.printStackTrace();
            }
        });

        loadData.setOnAction(event -> {
             data = ParseUtils.getData(file);
             table.setItems(data);
             table.setVisible(true);
        });

        Button button1 = new Button("Choose files");
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent e) {
                List<File> list =
                        fileChooser.showOpenMultipleDialog(stage);
                if (list != null) {
                    for (File file : list) {
                        System.out.println(file);
                        //openFile(file);
                    }
                }
            }
        });

        vBox.getChildren().addAll(heading, hBox, hBox1, table);
        vBox.setPadding(new Insets(10, 10, 10, 10));
        vBox.setSpacing(20);
        stage.setScene(scene);
        stage.initOwner(parent);
        stage.initStyle(StageStyle.UTILITY);
        stage.showAndWait();
    }

    private void contructTablePanel() {
        TableColumn question = new TableColumn("Question");
        question.setMinWidth(400);
        question.setCellValueFactory(new PropertyValueFactory<Question, String>("question"));

        TableColumn options = new TableColumn("Options");
        options.setMinWidth(100);
        options.setCellValueFactory(new PropertyValueFactory<Question, String[]>("options"));

        TableColumn question_type = new TableColumn("Question Type");
        question_type.setMinWidth(200);
        question_type.setCellValueFactory(new PropertyValueFactory<Question, String>("questionType"));

        TableColumn image = new TableColumn("Image");
        image.setMinWidth(200);
        image.setCellValueFactory(new PropertyValueFactory<Question, String>("imagePath"));

        TableColumn solution = new TableColumn("Solution");
        solution.setMinWidth(200);
        solution.setCellValueFactory(new PropertyValueFactory<Question, String[]>("solution"));

        TableColumn status = new TableColumn("Status");
        status.setMinWidth(200);
        status.setCellValueFactory(new PropertyValueFactory<Question, String>("status"));

        TableColumn answered = new TableColumn("Answered");
        answered.setMinWidth(200);
        answered.setCellValueFactory(new PropertyValueFactory<Question, String>("isAnswer"));

        TableColumn questionType = new TableColumn("Question Type");
        questionType.setMinWidth(20);
        questionType.setCellValueFactory(new PropertyValueFactory<Question, String>("type"));

        TableColumn number = new TableColumn("No");
        number.setMinWidth(20);
        number.setCellValueFactory(new PropertyValueFactory<Question, String>("number"));

        table.getColumns().addAll(number, question, options, questionType);
    }

}
