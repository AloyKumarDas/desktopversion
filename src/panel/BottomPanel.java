package panel;

import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.HBox;

import java.io.FileInputStream;

public class BottomPanel {

    private String[] socialMediaList = {"facebook", "instagram", "Tweeter", "whatsApp", "Support"};
    private HBox ui;
    AnchorPane anchorPane;
    public BottomPanel(){
        constructUI();
    }

    private void constructUI(){
        ui = new HBox();
        anchorPane = new AnchorPane(ui);
        AnchorPane.setLeftAnchor(ui, 50.0);
        AnchorPane.setRightAnchor(ui, 50.0);
        anchorPane.setStyle("-fx-border-color: grey; -fx-border-width: 1px 1px 1px 0px");

        for(int i = 0; i < socialMediaList.length; i++){
            HBox hBox = new HBox();
            try{
                FileInputStream fileInputStream = new FileInputStream(getIcon(socialMediaList[i]));
                Image image = new Image(fileInputStream);
                ImageView imageView = new ImageView(image);
                //Label socialMediaName = new Label(socialMediaList[i]);
                Hyperlink socialMediaName = new Hyperlink(socialMediaList[i]);
                hBox.getChildren().addAll(imageView, socialMediaName);
                hBox.setSpacing(5);
                ui.getChildren().add(hBox);
            }catch (Exception e){
                System.out.println(e);
            }
        }
        ui.setPadding(new Insets(4,100,2,100));
        ui.setSpacing(100);
    }

    private String getIcon(String name){
        String iconName = "";
        switch (name){
            case "facebook":
                iconName = "C:\\Users\\USER\\IdeaProjects\\ShopManagement\\src\\icons\\facebook.png";
                break;
            case "instagram":
                iconName = "C:\\Users\\USER\\IdeaProjects\\ShopManagement\\src\\icons\\instagram-sketched.png";
                break;
            case "Tweeter":
                iconName = "C:\\Users\\USER\\IdeaProjects\\ShopManagement\\src\\icons\\facebook.png";
                break;
            case "whatsApp":
                iconName = "C:\\Users\\USER\\IdeaProjects\\ShopManagement\\src\\icons\\whatsapp.png";
                break;
            default:
                iconName = "C:\\Users\\USER\\IdeaProjects\\ShopManagement\\src\\icons\\gmail.png";
                break;
        }

        return iconName;
    }

    public AnchorPane getUi() {
        return anchorPane;
    }
}
