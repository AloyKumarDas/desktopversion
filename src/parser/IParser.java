package parser;

import javafx.collections.ObservableList;
import model.Question;

public interface IParser {
    public ObservableList<Question> getData();
    public void constructData();
}
