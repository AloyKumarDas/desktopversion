package parser;

import constants.FileConstant;
import javafx.collections.ObservableList;
import model.Question;

import java.io.File;

public class ParseUtils {

    public static ObservableList<Question> getData(File file){
        IParser parser = null;
        if(file.getName().endsWith(FileConstant.CSV)){
            parser = new ParseExcelData(file.getPath());
        }
        parser.constructData();
        return parser.getData();
    }
}
