package parser;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Question;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ParseExcelData implements IParser {

    private String path;
    private ObservableList<Question> data;

    ParseExcelData(String path){
        this.path = path;
        constructData();
    }


    @Override
    public ObservableList<Question> getData() {
        return data;
    }

    @Override
    public void constructData() {
        data = FXCollections.observableArrayList();
        try {
            File file = new File(path);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            int number = 1;
            while ((line = br.readLine()) != null) {
                data.add(constructObject(line, number));
                number++;
            }
        }catch (Exception exp) {
            System.out.println(exp.getMessage());
        }
    }

    private Question constructObject(String line, int number){
        String[] info = line.split(",");
        Question question = new Question();
        question.setQuestion(info[0]);
        question.setOptions(info[1].split("\t"));
        question.setType(info[info.length-1]);
        question.setNumber(number);
        return question;
    }

}
