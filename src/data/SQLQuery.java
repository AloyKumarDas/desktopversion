package data;

import constants.DBConstant;

public class SQLQuery {

    public static String getCreateTableQuery(String tableName, String type){

        if(type.equals(DBConstant.QUES_SET))
            return  "CREATE TABLE IF NOT EXISTS " + tableName + "(\n"
                + "	id integer PRIMARY KEY,\n"
                + "	question text NOT NULL,\n"
                + "	options text\n"
                + "	questionType text\n"
                + "	imagePath blob\n"
                + ");";
        else
            return  "CREATE TABLE IF NOT EXISTS " + tableName + "(\n"
                + "	id integer PRIMARY KEY,\n"
                + " questionName text,\n"
                + " numberOfQuestion integer,\n"
                + " testDuration integer,\n"
                + " numberOfSection integer,\n"
                + " difficultyLevel text,\n"
                +")";

    }
}
