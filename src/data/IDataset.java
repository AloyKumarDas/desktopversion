package data;

import java.sql.ResultSet;

public interface IDataset {

    public void insertData(String tableName);

    public void updateTable(String query);

    public void createTable(String tableName);

    public ResultSet getTableData(String tableName);

}
