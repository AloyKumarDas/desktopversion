package data;

import utils.PropertyUtils;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

public class Database implements IDatabase {

    private String dbName;
    private static Logger logger = Logger.getLogger(Database.class.getName());
    public Database(String dbName){
        this.dbName = dbName;
    }

    @Override
    public void createDatabase() {

        Properties properties = PropertyUtils.getProperties();
        String dbPath = properties.getProperty("db.path") + File.separator + this.dbName;
        String url = properties.getProperty("db.driver") + dbPath;
        File file = new File(dbPath);

        if (file.exists()) {
            logger.info("This database name already exists.");
        } else {
            try {
                Connection conn = DriverManager.getConnection(url);
                if (conn != null) {
                    DatabaseMetaData meta = conn.getMetaData();
                    logger.info("The driver name is " + meta.getDriverName());
                    logger.info("Database created :- " + dbName);
                }

            } catch (SQLException e) {
                logger.info("Error in creating DB");
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public Connection connect() {
        Properties properties = new Properties();
        String url = properties.getProperty("db.driver")+ File.separator + this.dbName;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println("Db connection error " + e.getMessage());
        }
        return conn;
    }
}
