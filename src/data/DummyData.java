package data;

import constants.ButtonConstant;
import constants.DummyConstant;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.QuestionInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyData {
    private Map<String , String[]> userSubTopic;
    private Map<String , String[]> onlineSubTopic;
    private Map<String , String[]> practiceSubTopic;

    public DummyData(){
        initializeVariable();
    }

    private void initializeVariable(){
        practiceSubTopic = new HashMap<>();
        String[] practiceSets = getTopiceList(ButtonConstant.PRACTICE_SETS);

        practiceSubTopic.put(practiceSets[0], DummyConstant.GENERAL_APTITUDE);
        practiceSubTopic.put(practiceSets[1], DummyConstant.DATA_INTERPRETATION);
        practiceSubTopic.put(practiceSets[2], DummyConstant.VER_ABIL);

        onlineSubTopic = new HashMap<>();
        String[] onlineSets = getTopiceList(ButtonConstant.ONLINE_SETS);
        for(int i = 0; i < onlineSets.length; i++) {
            onlineSubTopic.put(onlineSets[i], DummyConstant.LEVEL);
        }

        userSubTopic = new HashMap<>();
        String[] userSets = getSubTopicMap(ButtonConstant.USER_SETS);
        for(int i = 0; i < userSets.length; i++) {
            userSubTopic.put(userSets[i], DummyConstant.LEVEL);
        }

    }

    private static String[] getTopiceList(String topicName){
        if(topicName.equals(ButtonConstant.PRACTICE_SETS)){
            return DummyConstant.PRACTICE_TOPIC;
        }else if(topicName.equals(ButtonConstant.ONLINE_SETS)){
            return DummyConstant.ONLINE_TOPIC;
        }
        return DummyConstant.PRACTICE_TOPIC;
    }

    public static String[] getSubTopicMap(String topicName){
        if(topicName.equals(ButtonConstant.PRACTICE_SETS)){
            return DummyConstant.PRACTICE_TOPIC;
        }else if(topicName.equals(ButtonConstant.ONLINE_SETS)){
            return DummyConstant.ONLINE_TOPIC;
        }
        return DummyConstant.PRACTICE_TOPIC;
    }

    public ObservableList<QuestionInfo> getData(){
        //int questionNumber, int numberOfQuestion, int testDuration, int numberOfSection, String questionName, String difficultyLevel
        ObservableList<QuestionInfo> data = FXCollections.observableArrayList(
                new QuestionInfo(1, "ABC1",45, 40, 2, "Easy"),
                new QuestionInfo(2, "ABC2",45, 40, 2, "Easy"),
                new QuestionInfo(3, "ABC3",45, 40, 2, "Easy"),
                new QuestionInfo(4, "ABC4",45, 40, 2, "Easy"),
                new QuestionInfo(5, "ABC5",45, 40, 2, "Easy"),
                new QuestionInfo(6, "ABC6",45, 40, 2, "Easy"),
                new QuestionInfo(7, "ABC7",45, 40, 2, "Easy"),
                new QuestionInfo(8, "ABC8",45, 40, 2, "Easy"),
                new QuestionInfo(9, "ABC9",45, 40, 2, "Easy"),
                new QuestionInfo(10, "ABC10",45, 40, 2, "Easy"),
                new QuestionInfo(11, "ABC11",45, 40, 2, "Easy"),
                new QuestionInfo(12, "ABC12",45, 40, 2, "Easy"),
                new QuestionInfo(13, "ABC13",45, 40, 2, "Easy"),
                new QuestionInfo(14, "ABC14",45, 40, 2, "Easy"),
                new QuestionInfo(15, "ABC15",45, 40, 2, "Easy"),
                new QuestionInfo(16, "ABC16",90, 90, 3, "Hard"),
                new QuestionInfo(17, "ABC17",90, 90, 3, "Hard"),
                new QuestionInfo(18, "ABC18",90, 90, 3, "Hard"),
                new QuestionInfo(19, "ABC19",90, 90, 3, "Hard"),
                new QuestionInfo(20, "CDE20",90, 90, 3, "Hard"),
                new QuestionInfo(21, "fadfa21",90, 90, 3, "Hard"),
                new QuestionInfo(22, "kkkk22",90, 90, 3, "Hard"),
                new QuestionInfo(23, "bbb23",90, 90, 3, "Hard"),
                new QuestionInfo(24, "ttt24",90, 90, 3, "Hard"),
                new QuestionInfo(25, "ABC25",90, 90, 3, "Hard"),
                new QuestionInfo(26, "ABC26",90, 90, 3, "Hard"),
                new QuestionInfo(27, "mmmm26",90, 90, 3, "Medium"),
                new QuestionInfo(28, "nnnn26",90, 90, 3, "Medium"),
                new QuestionInfo(29, "gggg26",90, 90, 3, "Medium"),
                new QuestionInfo(30, "wwww26",90, 90, 3, "Medium"),
                new QuestionInfo(31, "ddd26",90, 90, 3, "Medium"),
                new QuestionInfo(32, "bbbb26",90, 90, 3, "Medium"),
                new QuestionInfo(33, "kkkk26",90, 90, 3, "Medium"),
                new QuestionInfo(34, "mmmm26",90, 90, 3, "Medium"),
                new QuestionInfo(35, "tttt26",90, 90, 3, "Medium"),
                new QuestionInfo(36, "llll26",90, 90, 3, "Medium"),
                new QuestionInfo(37, "khh26",90, 90, 3, "Medium"),
                new QuestionInfo(38, "fdsfsd26",90, 90, 3, "Medium"),
                new QuestionInfo(39, "kjkh26",90, 90, 3, "Medium"),
                new QuestionInfo(40, "fadfa26",90, 90, 3, "Medium"),
                new QuestionInfo(41, "CDE26",90, 90, 3, "Medium"),
                new QuestionInfo(42, "CDE26",90, 90, 3, "Medium"),
                new QuestionInfo(43, "CDE26",90, 90, 3, "Medium"),
                new QuestionInfo(44, "CDE26",60, 90, 3, "Medium")

                );

        return data;
    }

    public Map<String, String[]> getUserSubTopic() {
        return userSubTopic;
    }

    public Map<String, String[]> getOnlineSubTopic() {
        return onlineSubTopic;
    }

    public Map<String, String[]> getPracticeSubTopic() {
        return practiceSubTopic;
    }
}
