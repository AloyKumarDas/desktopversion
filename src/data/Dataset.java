package data;

import java.sql.*;

public class Dataset implements IDataset {

    private Connection connection;
    private String type;

    Dataset(Connection connection, String type){
        this.connection = connection;
        this.type = type;
    }

    @Override
    public void insertData(String tableName) {
        String sql = "INSERT INTO warehouses(name,capacity) VALUES(?,?)";
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            //pstmt.setString(1, name);
            //pstmt.setDouble(2, capacity);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Insert Data failed : "+e.getMessage());
        }
    }

    @Override
    public void updateTable(String query) {

    }

    @Override
    public void createTable(String tableName) {
        String sqlQuery = SQLQuery.getCreateTableQuery(tableName, type);
        try {
            Statement stmt = connection.createStatement();
            stmt.execute(sqlQuery);
        } catch (SQLException e) {
            System.out.println("Create table failed " + e.getMessage());
        }
    }

    @Override
    public ResultSet getTableData(String tableName) {
        String sql = "SELECT * FROM " + tableName;

        ResultSet resultSet = null;
        try {
            Statement stmt  = connection.createStatement();
            resultSet  = stmt.executeQuery(sql);
            /*while (rs.next()) {
                System.out.println(rs.getInt("id") +  "\t" +
                        rs.getString("name") + "\t" +
                        rs.getDouble("capacity"));
            }*/
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return resultSet;
    }
}
