package data;

import java.sql.Connection;

public interface IDatabase {

    public void createDatabase();

    public Connection connect();
}
