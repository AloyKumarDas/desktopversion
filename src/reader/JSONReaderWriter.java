package reader;

import constants.Constant;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.print.attribute.HashAttributeSet;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class JSONReaderWriter {

    public static JSONObject getJsonObject(String fileName){
        JSONObject jsonObject = null;
        try {
            Object obj = new JSONParser().parse(new FileReader(fileName+ Constant.JSON));
            jsonObject = (JSONObject) obj;
        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static void updateJSONFile(JSONObject jsonObject, String fileName){
        try {
            FileWriter file = new FileWriter(fileName+Constant.JSON);
            file.write(jsonObject.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
