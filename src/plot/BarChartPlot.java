package plot;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.util.Map;

public class BarChartPlot {

    private String[] sections, levels;
    private Map<String, Object> data;
    private BarChart<String,Number> barChart;
    private String title;

    public BarChartPlot(String[] sections, String[] levels, Map<String, Object> data){
        this.sections = sections;
        this.levels = levels;
        this.data = data;
        constructUI();
    }

    private void constructUI(){
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        barChart = new BarChart<String,Number>(xAxis,yAxis);
        xAxis.setLabel("Sections");
        yAxis.setLabel("Number of Questions");
        int[] list1 = (int[])data.get(sections[0]);
        int[] list2 = (int[])data.get(sections[1]);
        int[] list3 = (int[])data.get(sections[2]);

        for(int i = 0; i < levels.length; i++ ){
            XYChart.Series series = new XYChart.Series();
            series.setName(levels[i]);
            for (int j = 0; j < 3; j++){
                series.getData().add(new XYChart.Data(sections[0], list1[i]));
                series.getData().add(new XYChart.Data(sections[1], list2[i]));
                series.getData().add(new XYChart.Data(sections[2], list3[i]));
            }
            barChart.getData().add(series);
        }


    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BarChart<String, Number> getBarChart() {
        return barChart;
    }
}
