package media;

import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;

public class IconLoader {

    public static Image getToolIcon(){
        String src = "src"+File.separator+"images"+ File.separator +"icons8-file-20.png";
        Image toolIcon = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(src);
            toolIcon = new Image(fileInputStream);
        }catch (Exception e){
            e.printStackTrace();
        }
        return toolIcon;
    }

}
