package model;

public class QuestionInfo {
    private int questionNumber, numberOfQuestion, testDuration, numberOfSection;
    private String questionName, difficultyLevel;

    public QuestionInfo(int questionNumber, String questionName, int numberOfQuestion, int testDuration, int numberOfSection, String difficultyLevel) {
        this.questionNumber = questionNumber;
        this.numberOfQuestion = numberOfQuestion;
        this.testDuration = testDuration;
        this.numberOfSection = numberOfSection;
        this.questionName = questionName;
        this.difficultyLevel = difficultyLevel;

    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    public int getNumberOfQuestion() {
        return numberOfQuestion;
    }

    public void setNumberOfQuestion(int numberOfQuestion) {
        this.numberOfQuestion = numberOfQuestion;
    }

    public int getTestDuration() {
        return testDuration;
    }

    public void setTestDuration(int testDuration) {
        this.testDuration = testDuration;
    }

    public int getNumberOfSection() {
        return numberOfSection;
    }

    public void setNumberOfSection(int numberOfSection) {
        this.numberOfSection = numberOfSection;
    }

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public String getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(String difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    @Override
    public String toString() {
        return "QuestionInfo{" +
                "questionNumber=" + questionNumber +
                ", numberOfQuestion=" + numberOfQuestion +
                ", testDuration=" + testDuration +
                ", numberOfSection=" + numberOfSection +
                ", questionName='" + questionName + '\'' +
                ", difficultyLevel='" + difficultyLevel + '\'' +
                '}';
    }

    public String getOrderParameters(){
        return "()";
    }

}
