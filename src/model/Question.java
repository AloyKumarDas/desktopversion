package model;

import java.util.Arrays;

public class Question {
    private String question;
    private String[] options;
    private String questionType;
    private String imagePath;
    private Object[] solution;
    private String status;
    private boolean isAnswer;
    private String type;
    private int number;

    public Question(){

    }

    public Question(String question, String[] options, String questionType, String imagePath){
        this.question = question;
        this.options = options;
        this.questionType = questionType;
        this.imagePath = imagePath;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Object[] getSolution() {
        return solution;
    }

    public void setSolution(Object[] solution) {
        this.solution = solution;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isAnswer() {
        return isAnswer;
    }

    public void setAnswer(boolean answer) {
        isAnswer = answer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Question{" +
                "question='" + question + '\'' +
                ", options=" + Arrays.toString(options) +
                ", questionType='" + questionType + '\'' +
                ", imagePath='" + imagePath + '\'' +
                ", solution=" + Arrays.toString(solution) +
                ", status='" + status + '\'' +
                '}';
    }
}
