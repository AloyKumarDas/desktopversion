package sample;


import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class TabularView {
    private HBox hBox;
    private VBox vBox;
    private TableView tableView;
    private Button searchButton, startButton, resetButton;


    public TabularView(){
    }

    public TabularView(TableView tableView){
        this.tableView = tableView;
        constructTableView();
    }

    private void initializeVariable(){
        hBox = new HBox();
        vBox = new VBox();
        searchButton = new Button("Search");
        startButton = new Button("Start");
        resetButton = new Button("Reset");
    }

    private void constructTableView(){
        initializeVariable();
        hBox.getChildren().addAll(resetButton, searchButton, startButton);
        vBox.getChildren().addAll(tableView, hBox);
    }


    public VBox getUI(){
        return vBox;
    }
}
