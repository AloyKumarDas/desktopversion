package sample;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Font;
import view.TableViewPanel;

public class CustomTab {
    private Tab tab;
    private String type;
    private boolean hasChildren= false;
    private Scene scene;

    public CustomTab(){

    }

    public CustomTab(String type, boolean hasChildren){
        this.type = type;
        tab = new Tab(type);
        this.hasChildren = hasChildren;
    }

    public CustomTab(Scene scene, String type, boolean hasChildren){
        this(type, hasChildren);
        this.scene = scene;
    }

    private Tab constructTab(){
        TableViewPanel tableViewPanel = new TableViewPanel(scene, type);

        TabularView tabularView = null;
        if(!type.equals("Start")) {
            if (!hasChildren) {
                tab.setContent(tableViewPanel.getTabPanVbox());
            } /*else {
                QuestionDetails questionDetails = new QuestionDetails();
                tab.setContent(questionDetails.getGridPane());
            }*/
        }else {
            Label label = new Label("Introduction Page");
            label.setFont(Font.font(30));
            tab.setContent(label);
        }
        //tab.setOnClosed(TabPane.TabClosingPolicy.UNAVAILABLE);

        return tab;
     }

    public Tab getTab(){
        constructTab();
        return tab;
    }
}
