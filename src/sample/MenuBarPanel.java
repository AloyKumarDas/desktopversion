package sample;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import panel.MenuUI;

public class MenuBarPanel {
    private Scene scene;
    private VBox vBox;
    private Stage parent;

    public MenuBarPanel(){

    }

    public MenuBarPanel(Stage parent, Scene scene){
        this.scene = scene;
        this.parent = parent;
        constructUI();
    }

    private void init(){
        vBox = new VBox();
    }

    private void constructUI(){
        init();
        Menu set = new Menu("_Sets");
        MenuItem newSet = new MenuItem("New Set");
        set.getItems().add(newSet);

        newSet.setOnAction(e -> {
            e.consume();
            showUI(newSet.getText());
        });


        Menu performance = new Menu("_Performance");

        Menu search = new Menu("_Search");
        Menu helpmenu = new Menu("_Help");
        CheckMenuItem checkMenuItem = new CheckMenuItem("Checkbox menu item");
        helpmenu.getItems().addAll(checkMenuItem);

        MenuBar menuBar = new MenuBar();
        menuBar.paddingProperty().setValue(new Insets(0, 0, 0, -2));
        menuBar.getMenus().addAll(set, search, performance, helpmenu);



        vBox.getChildren().addAll(menuBar);
    }

    private void showUI(String name){
        MenuUI menuUI = new MenuUI(parent, name);
        menuUI.constructUI();
    }

    public VBox getUI(){
        return vBox;
    }
}
