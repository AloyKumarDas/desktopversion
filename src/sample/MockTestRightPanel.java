package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MockTestRightPanel {
    private FlowPane flowPane;
    private Stage stage;
    private Scene scene;


    // Variable declare for timer
    private Integer minute = 2;
    private static final Integer STARTTIME = 00;
    private Timeline timeline;
    private Label timerLabel = new Label();
    private Integer timeSeconds = STARTTIME;

    private VBox vBox;

    public MockTestRightPanel(){
        constructMockTestPane();
    }

    public MockTestRightPanel(Stage stage, Scene scene){
        this.stage = stage;
        this.scene = scene;
        constructMockTestPane();
    }

    private void constructMockTestPane() {
        vBox = new VBox();
        vBox.setMaxWidth(200);
        // Configure the Label
        timerLabel.setText(minute.toString() +" : "+ timeSeconds.toString());
        timerLabel.setTextFill(Color.GREEN);
        timerLabel.setStyle("-fx-font-size: 4em;");
        // Create and configure the Button
        Button button = new Button();
        button.setText("Start Timer");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(timeline != null){
                    timeline.stop();
                }
                timeSeconds = STARTTIME;
                timeline = new Timeline();
                timeline.setCycleCount(Timeline.INDEFINITE);
                timeline.getKeyFrames().add(
                        new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                System.out.println("Before "+ timeSeconds);
                                timeSeconds--;
                                if(timeSeconds < 0){
                                    minute--;
                                    timeSeconds = 59;
                                }
                                // Change color
                                if(minute == 0 && timeSeconds < 30){
                                    timerLabel.setTextFill(Color.RED);
                                }

                                // Change color
                                System.out.println("After "+ timeSeconds);
                                timerLabel.setText(minute.toString() + " : "+timeSeconds.toString());
                                if(minute == 0 && timeSeconds <=0){
                                    timeline.stop();
                                }
                                if(minute < 0 || timeSeconds < 0 )
                                    timeline.stop();
                            }
                        })
                );
                timeline.playFromStart();
            }
        });

        // Flow pane code
        flowPane = new FlowPane();
        FlowPane flowPane = new FlowPane(Orientation.HORIZONTAL, 5, 5);
        flowPane.setPadding(new Insets(5,5,5,5));

        for (int i=1; i<=20; i++) {
            flowPane.getChildren().add(getButton(i));
        }

        // Question Marking button
        Button unAnsered = getButton(0);

        Button ansered = getButton(0);
        Button markedForLater = getButton(0);

        vBox.setSpacing(5);
        vBox.getChildren().addAll(button, timerLabel,flowPane, unAnsered, ansered, markedForLater);
    }

    private Button getButton(int index){
        Button bt = new Button(Integer.toString(index));
        bt.setOnAction(e -> clickAction(bt));
        bt.setShape(new Circle(1.5));
        //bt.setShape(new Polygon(5));
        bt.setMaxSize(3,3);
        bt.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 40px; " +
                        "-fx-min-height: 40px; " +
                        "-fx-max-width: 40px; " +
                        "-fx-max-height: 40px;"
        );
        return bt;
    }

    private void clickAction(Button bt) {
        System.out.println(bt.getText());
    }

    public VBox getPanel(){
        return vBox;
    }
}
