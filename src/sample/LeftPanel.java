package sample;

import constants.ButtonConstant;
import constants.Constant;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import view.ConstructTreeItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LeftPanel {
    private VBox vBox;
    private HBox hBox;
    private Scene scene;
    private TreeView treeView;
    private TreeItem treeItem;
    private ConstructTreeItem constructTreeItem;
    private ComboBox<String> subjectsSubsetComboBox;
    private ComboBox<String> subjectsListComboBox;

    public LeftPanel(Scene scene){
        this.scene = scene;
        vBox = new VBox();
        constructUI();
    }

    private TreeView getTreeView(){
        treeView = new TreeView();
        treeView.setMinHeight(600);
        treeView.setMaxHeight(Double.MAX_VALUE);
        treeView.onMouseClickedProperty().set(new EventHandler<javafx.scene.input.MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent event) {
                TreeItem tempTreeItem = (TreeItem) treeView.getSelectionModel().getSelectedItem();
                if(event.getClickCount() == 2 && tempTreeItem.getChildren().size() == 0){
                    BorderPane bp = (BorderPane) scene.getRoot();
                    TabPane tabPane = (TabPane) ((VBox)bp.getCenter()).getChildren().get(0);
                    String nodeName = ((TreeItem)((TreeView)event.getSource()).getSelectionModel().getSelectedItem()).getValue().toString();

                    ObservableList observableList = tabPane.getTabs();
                    boolean isAvailable = false;
                    int index = -1;
                    int availableTab = observableList.size();
                    for(int i = 0; i < observableList.size(); i++){
                        String tabName  = ((Tab)observableList.get(i)).getText().toString();
                        if(tabName.equals(nodeName)){
                            isAvailable = true;
                            index = i;
                        }
                    }
                    boolean hasChildren = tempTreeItem.getChildren().size() == 0 ? false : true;
                    if(!isAvailable) {
                        CustomTab customTab = new CustomTab(scene, nodeName, hasChildren);
                        tabPane.getTabs().add(customTab.getTab());
                        if(index > -1 ){
                        }else{
                            tabPane.getSelectionModel().clearAndSelect(availableTab);
                        }
                    }else {
                        tabPane.getSelectionModel().clearAndSelect(index);
                    }
                }
            }
        });

        treeView.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        treeView.setBorder(Border.EMPTY);
        String s = "-fx-background-insets: null;";
        treeView.setStyle(s);
        if(treeItem == null){
            constructTreeItem = new ConstructTreeItem(ButtonConstant.PRACTICE_SETS);
            treeItem = constructTreeItem.getTreeItem();
        }
        treeView.setRoot(treeItem);

        // To hide the root item in tree view
        treeView.setShowRoot(false);
        treeView.setMaxHeight(Double.MAX_VALUE);

        return treeView;
    }

    private void constructUI(){

        subjectsListComboBox = new ComboBox<>();
        subjectsListComboBox.setPromptText("Choose subject");
        subjectsListComboBox.getItems().addAll(Constant.subjects);
        subjectsListComboBox.setMinWidth(150);
        subjectsListComboBox.setMaxWidth(Double.MAX_VALUE);

        subjectsSubsetComboBox = new ComboBox<>();
        subjectsSubsetComboBox.setPromptText("Topic List");
        subjectsSubsetComboBox.setMinWidth(150);
        subjectsSubsetComboBox.setMaxWidth(Double.MAX_VALUE);

        subjectsListComboBox.setOnAction(e -> {
            String option = subjectsListComboBox.getValue();
            subjectsSubsetComboBox.getItems().clear();
            if(option.equals(Constant.ENGLISH)){
                subjectsSubsetComboBox.getItems().addAll(Constant.EnglishSubList);
            }else if(option.equals(Constant.MATH)){
                subjectsSubsetComboBox.getItems().addAll(Constant.MathSubList);
            }
            constructTreeItem.updateTreeItem(treeView, subjectsListComboBox.getValue(), subjectsSubsetComboBox.getValue());
        });


        subjectsSubsetComboBox.setOnAction(e -> {
            constructTreeItem.updateTreeItem(treeView, subjectsListComboBox.getValue(), subjectsSubsetComboBox.getValue());
        });


        VerticalButtonPanel verticalButtonPanel = new VerticalButtonPanel(scene, this, ButtonConstant.LEFT_BUTTON_LIST);

        treeView = getTreeView();
        vBox.getChildren().addAll(subjectsListComboBox, subjectsSubsetComboBox, treeView);
        vBox.setMaxHeight(Double.MAX_VALUE);
        String style = "-fx-background-color: rgba(0, 0, 0, .1);";


        //vBox.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

        Paint borderPaint = Color.gray(0.1);
        //vBox.setBackground(new BackgroundFill(borderPaint));
        //vBox.setFillWidth(true);
        vBox.setStyle("-fx-border-color: grey;");
        /*double   borderWidth = 2;
        vBox.setBorder(new Border(new BorderStroke(borderPaint, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(borderWidth))));
        vBox.prefWidthProperty().bind(scene.widthProperty().multiply(0.20));*/
//        vBox.maxWidthProperty().bind(vBox.widthProperty());
        //vBox.autosize();

        hBox = new HBox();
        hBox.getChildren().addAll(verticalButtonPanel.getUI(), vBox);
    }

    public HBox getLeftPanel(){
        return hBox;
    }

    public void setTreeItem(TreeItem treeItem) {
        treeView.setRoot(treeItem);
        this.treeItem = treeItem;
    }

    public ComboBox<String> getSubjectsListComboBox() {
        return subjectsListComboBox;
    }

    public void setSubjectsListComboBox(ComboBox<String> subjectsListComboBox) {
        this.subjectsListComboBox = subjectsListComboBox;
    }

    public void updateTopPanel(){

    }
}
