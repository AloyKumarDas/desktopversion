package sample;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import view.ConstructTreeItem;

import java.util.ArrayList;
import java.util.List;

public class VerticalButtonPanel {

    private Scene scene;
    private VBox leftButtonBox;
    private List<Button> buttonList = new ArrayList<>();
    private String[] buttonTextList;
    private LeftPanel leftPanel;
    private RightPanel rightPanel;
    private String activeButtonText;

    public VerticalButtonPanel(){}

    private VerticalButtonPanel(Scene scene, String[] buttonTextList){
        this.scene = scene;
        this.buttonTextList = buttonTextList;
        constructPanel();
    }

    public VerticalButtonPanel(Scene scene, RightPanel rightPanel, String[] buttonTextList){
        this(scene, buttonTextList);
        this.rightPanel = rightPanel;
    }

    public VerticalButtonPanel(Scene scene, LeftPanel leftPanel, String[] buttonTextList){
        this(scene, buttonTextList);
        this.leftPanel = leftPanel;
    }

    private void constructPanel(){
        leftButtonBox = new VBox();
        leftButtonBox.setSpacing(1);
        for(int i = 0; i <  buttonTextList.length; i++){
            Button button = createButtonWithRotatedText(buttonTextList[i], i+1);
            button.setMinWidth(22);
            button.setOnAction(e -> changePanelValue(button));
            buttonList.add(button);
            leftButtonBox.getChildren().add(button);
        }
        activeButtonText = getButtonText(buttonList.get(0));
    }

    private Button createButtonWithRotatedText(String text, int index) {
        Button button = new Button();
        button.setFocusTraversable(true);
        Label label  = new Label(Integer.toString(index) +":"+text);
        label.setRotate(-90);
        button.setGraphic(new Group(label));
        button.setFont(Font.font(8));

        // in-line css just used for sample purposes,
        // usually you would apply a stylesheet.
        /*button.setStyle(
                "-fx-base: gray; " +
                        "-fx-font-size: 10px; " +
                        "-fx-text-background-color: whitesmoke;"
        );*/

        return button;
    }

    private void changePanelValue(Button button){
        String sideBarButtonText = getButtonText(button);
        if(leftPanel != null && !activeButtonText.equals(sideBarButtonText)){
            ConstructTreeItem constructTreeItem = new ConstructTreeItem(sideBarButtonText);
            leftPanel.setTreeItem(constructTreeItem.getTreeItem());
            leftPanel.updateTopPanel();
            activeButtonText = sideBarButtonText;
        } else if(rightPanel != null) {
            System.out.println("Click " + rightPanel);
        }
    }

    private String getButtonText(Button button){
        String sideBarButtonText = ((Label)((Group)button.getGraphic()).getChildren().get(0)).getText();
        String[] ss = sideBarButtonText.split(":");
        return ss[1];
    }

    public VBox getUI(){
        return leftButtonBox;
    }

}
