package sample;

import constants.ButtonConstant;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class RightPanel {
    private VBox vBox;
    private HBox hBox;
    private Scene scene;

    public RightPanel(){

    }

    public RightPanel(Scene scene) {
        this.scene = scene;
        constructUI();
    }

    private void initialize(){
        vBox = new VBox();
        hBox = new HBox();
    }

    private void constructUI() {
        initialize();
        VerticalButtonPanel verticalButtonPanel = new VerticalButtonPanel(scene, this, ButtonConstant.RIGHT_BUTTON_LIST);
        vBox.getChildren().add(verticalButtonPanel.getUI());
    }

    public VBox getUI(){
        return vBox;
    }

}

