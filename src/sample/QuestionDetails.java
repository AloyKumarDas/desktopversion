package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.Map;

public class QuestionDetails {
    private VBox vBox;
    private HBox hBox;
    private Map<String, Object> details;
    private GridPane gridPane;
    private Button startButton;

    public QuestionDetails(){

    }

    public QuestionDetails(Map details){
        this.details = details;
    }

    private void initializeVariable(){
        vBox = new VBox();
        hBox = new HBox();
        gridPane = new GridPane();
    }

    private void constructQuestionDetailsUI(){
        initializeVariable();
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data("Grapefruit", 13),
                        new PieChart.Data("Oranges", 25),
                        new PieChart.Data("Plums", 10),
                        new PieChart.Data("Pears", 22),
                        new PieChart.Data("Apples", 30));
        PieChart chart = new PieChart(pieChartData);
        chart.setLegendSide(Side.RIGHT);
        chart.setMinHeight(300);
        chart.setMaxHeight(300);
        chart.setTitle("Imported Fruits");
        Label heading = new Label("Heading !!! : ");
        Label type = new Label("Type : ");
        Label numberOfQuestion = new Label("Total Question : ");
        Label details = new Label("Details");

        gridPane.addRow(0, heading);
        gridPane.addRow(1, type);
        gridPane.addRow(2, numberOfQuestion);
        gridPane.addRow(3, details);
        //gridPane.addRow(3, details);

        gridPane.add(chart, 1,4);

    }

    public GridPane getGridPane() {
        constructQuestionDetailsUI();
        return gridPane;
    }
}
