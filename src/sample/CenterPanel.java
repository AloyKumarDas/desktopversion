package sample;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CenterPanel {
    private Stage stage;
    private Scene scene;
    private TabPane tabPane;
    private VBox vBox, rightPane;

    CenterPanel(Scene scene){
        this.scene = scene;
        vBox = new VBox();
        tabPane = new TabPane();
        constructCenterPanel();
    }

    private void constructCenterPanel() {
        CustomTab customTab = new CustomTab("Start", false);

        tabPane.paddingProperty().set(new Insets(-5,0,0,-5));
        tabPane.getTabs().add(customTab.getTab());
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
        tabPane.getTabs().get(0).setClosable(false);
        // Set tab max height
        //tabPane.setTabMaxHeight(Double.MAX_VALUE);
        vBox.setPadding(new Insets(0,5,0,5));
        vBox.getChildren().add(tabPane);
    }

    public VBox getCenterPanel(){
        return vBox;
    }
}
