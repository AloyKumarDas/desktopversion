package sample;

import constants.DBConstant;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import media.IconLoader;
import panel.BottomPanel;
import utils.DatasetUtils;

import java.io.FileInputStream;
import java.util.logging.Logger;

public class Main extends Application {

    private static Logger logger = Logger.getLogger(Main.class.getName());
    static {
        logger.info("Basic setup started.");
        DatasetUtils.basicDBSetup(DBConstant.DB);
        logger.info("Basic Setup ends.");
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Shop Management");

        primaryStage.getIcons().add(IconLoader.getToolIcon());
        BorderPane borderPane = new BorderPane();

        Scene scene = new Scene(borderPane, 1000, 600);

        MenuBarPanel menuBarPanel = new MenuBarPanel(primaryStage, scene);
        borderPane.setTop(menuBarPanel.getUI());

        String styleSheet = getClass().getResource("style.css").toExternalForm();
        scene.getStylesheets().add(styleSheet);

        LeftPanel leftPanel = new LeftPanel(scene);
        HBox tempvBox = leftPanel.getLeftPanel();
        //tempvBox.setMaxWidth(200);
        borderPane.setLeft(tempvBox);

        CenterPanel centerPanel = new CenterPanel(scene);
        borderPane.setCenter(centerPanel.getCenterPanel());

        RightPanel rightPanel = new RightPanel(scene);
        borderPane.setRight(rightPanel.getUI());

        BottomPanel bottomPanel = new BottomPanel();
        borderPane.setBottom(bottomPanel.getUi());

        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.UNIFIED);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
