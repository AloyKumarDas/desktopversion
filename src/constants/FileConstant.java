package constants;

public class FileConstant {
    public static String CSV = ".csv";
    public static String TSV = ".tsv";
    public static String TXT = ".txt";
    public static String JSON = ".json";
}
