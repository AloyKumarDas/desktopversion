package constants;

public class Constant {

    public static String PRACTICE = "practice";
    public static String ONLINE = "online";
    public static String USER = "user";
    public static String JSON = ".json";
    public static String ENGLISH = "English";
    public static String MATH = "Maths";
    public static String ALL = "All";

    public static String subjects[] = { "Maths", "English", "All"};

    public static String MathSubList[] = { "MathTopic_1", "MathTopic_2", "MathTopic_3", "MathTopic_4", "All"};

    public static String EnglishSubList[] = { "EnglishTopic_1", "EnglishTopic_2", "EnglishTopic_3", "EnglishTopic_4", "All" };

    /*public static String[] GRAMMAR = { "Adjectives", "Adverbs", "Interrogative", "Nouns", "Punctuation", "Verbs and Tenses" };

    public static String[] VER_ABIL = { "Spotting Errors", "Antonyms", "Spellings", "Ordering of Words",
            "Sentence Improvement", "Ordering of Sentences", "Closet Test", "One Word Substitutes",
            "Change of Voice", "Verbal Analogies", "Synonyms", "Selecting Words", "Sentence Formation",
            "Sentence Correction", "Completing Statements", "Paragraph Formation", "Comprehension",
            "Idioms and Phrases", "Change of Speech" };

    public static String[] READING_COMP = { "Summarizing", "Sequencing", "Inferencing", "Comparing and contrasting",
                                    "Drawing conclusions", "Self-questioning", "Problem-solving" };


    public static String[] PRACTICE_TOPIC = { "Arithmetic Aptitude", "Data Interpretation",
            "Verbal Ability", "General Knowledge", "English" };

    public static String[] ONLINE_TOPIC = { "Aptitude Test", "Verbal Ability Test", "Logical Reasoning Test",
            "Verbal Reasoning Test", "Non Verbal Reasoning Test", "Data Interpretation Test",
            "General Knowledge Test", "Current Affairs Test" };

    public static String[] GENERAL_APTITUDE = {"Numbers", "HCF and LCM of Numbers", "Decimal Fractions", "Square roots & cube roots",
            "Average", "Problems on ages", "Percentage", "Profit & loss"};

    public static String[] DATA_INTERPRETATION = {"Table Charts", "Pie Charts", "Bar Charts", "Line Charts"};

    public static String[] LEVEL = {"Easy Test", "Medium Test", "Difficult Test"};
*/
}
