package constants;

public class QuestionConstant {
    public static final String SAVE_AND_NEXT = "Save and Next";
    public static final String MARK_AND_REVIEW = "Marks for review & Next";
    public static final String CLEAR_RESPONSE = "Clear Response";
    public static final String SUBMIT = "Submit";

    public static final String ANSWERED =  "Answered";

    public static final String NOT_ANSWERED =  "Not answered";

    public static final String NOT_VISITED =  "Not visited";

    public static final String MARK_FOR_REVIEW =  "Mark for review";

    public static final String ANSWERED_AND_REVIEW = "Answered and review";
}
