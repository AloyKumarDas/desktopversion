package constants;

import java.util.HashMap;
import java.util.Map;

public class ConstantData {

    private Map<String , String[]> userSubTopic;
    private Map<String , String[]> onlineSubTopic;
    private Map<String , String[]> practiceSubTopic;

    public ConstantData(){
        initializeVariable();
    }

    private void initializeVariable(){
        practiceSubTopic = new HashMap<>();
        String[] practiceSets = getTopiceList(ButtonConstant.PRACTICE_SETS);

        practiceSubTopic.put(practiceSets[0], DummyConstant.GENERAL_APTITUDE);
        practiceSubTopic.put(practiceSets[1], DummyConstant.DATA_INTERPRETATION);
        practiceSubTopic.put(practiceSets[2], DummyConstant.VER_ABIL);

        onlineSubTopic = new HashMap<>();
        String[] onlineSets = getTopiceList(ButtonConstant.ONLINE_SETS);
        for(int i = 0; i < onlineSets.length; i++) {
            onlineSubTopic.put(onlineSets[i], DummyConstant.LEVEL);
        }

        userSubTopic = new HashMap<>();
        String[] userSets = getSubTopicMap(ButtonConstant.USER_SETS);
        for(int i = 0; i < userSets.length; i++) {
            userSubTopic.put(userSets[i], DummyConstant.LEVEL);
        }

    }

    private String[] getTopiceList(String topicName){
        if(topicName.equals(ButtonConstant.PRACTICE_SETS)){
            return DummyConstant.PRACTICE_TOPIC;
        }else if(topicName.equals(ButtonConstant.ONLINE_SETS)){
            return DummyConstant.ONLINE_TOPIC;
        }
        return DummyConstant.PRACTICE_TOPIC;
    }

    public String[] getSubTopicMap(String topicName){
        if(topicName.equals(ButtonConstant.PRACTICE_SETS)){
            return DummyConstant.PRACTICE_TOPIC;
        }else if(topicName.equals(ButtonConstant.ONLINE_SETS)){
            return DummyConstant.ONLINE_TOPIC;
        }
        return DummyConstant.PRACTICE_TOPIC;
    }

    public Map<String, String[]> getUserSubTopic() {
        return userSubTopic;
    }

    public Map<String, String[]> getOnlineSubTopic() {
        return onlineSubTopic;
    }

    public Map<String, String[]> getPracticeSubTopic() {
        return practiceSubTopic;
    }

}
