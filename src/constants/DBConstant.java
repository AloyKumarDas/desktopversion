package constants;

public class DBConstant {

    public static String MATH_DB = "MathDB";
    public static String ENGLISH_DB = "EnglishDB";
    public static String QUES_SETS_INFO = "QSetsInfo";
    public static String QUES_SET = "QSet";
    public static String DB = "data";
    public static String INFO_FILE = "info.properties";
}
