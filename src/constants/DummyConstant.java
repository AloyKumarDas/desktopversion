package constants;

import java.util.HashMap;
import java.util.Map;

public class DummyConstant {

    public static String[] PRACTICE_TOPIC = { "Arithmetic Aptitude", "Data Interpretation",
            "Verbal Ability", "General Knowledge", "English" };

    public static String[] ONLINE_TOPIC = { "Aptitude Test", "Verbal Ability Test", "Logical Reasoning Test",
            "Verbal Reasoning Test", "Non Verbal Reasoning Test", "Data Interpretation Test",
            "General Knowledge Test", "Current Affairs Test" };

    public static String[] GENERAL_APTITUDE = {"Numbers", "HCF and LCM of Numbers", "Decimal Fractions", "Square roots & cube roots",
        "Average", "Problems on ages", "Percentage", "Profit & loss"};

    public static String[] DATA_INTERPRETATION = {"Table Charts", "Pie Charts", "Bar Charts", "Line Charts"};

    public static String[] VER_ABIL = {"Spotting Errors", "Antonyms", "Spellings", "Ordering of Words",
            "Sentence Improvement", "Ordering of Sentences", "Closet Test", "One Word Substitutes",
            "Change of Voice", "Verbal Analogies", "Synonyms", "Selecting Words", "Sentence Formation",
            "Sentence Correction", "Completing Statements", "Paragraph Formation", "Comprehension",
            "Idioms and Phrases", "Change of Speech"};

    public static String[] LEVEL = {"Easy Test", "Medium Test", "Difficult Test"};

    public static Map<String, String[]> practice = new HashMap<>();

    public static Map<String, String[]> getPractice() {
        //practice.put();
        return practice;
    }
}
