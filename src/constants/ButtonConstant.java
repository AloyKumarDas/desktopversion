package constants;

public class ButtonConstant {

    public static String[] LEFT_BUTTON_LIST = {"Practice Sets", "Online Sets", "User Sets"};
    public static String[] RIGHT_BUTTON_LIST = {"Articles", "Important Notes", "Hot Topics"};
    public static final String PRACTICE_SETS = "Practice Sets";
    public static final String ONLINE_SETS = "Online Sets";
    public static final String USER_SETS = "User Sets";
    public static final String SEARCH = "Search . .";


}
