package constants;

public class ColorConstant {
    public static final String[] COLOR_LIST = {"#80d9bd", "#d61c38", "#d4d2cf", "#4760b3","#da35f0"};

    public static final String CIRCLE_BUTTON = "-fx-background-radius: 5em; " +
            "-fx-min-width: 40px; " +
            "-fx-min-height: 40px; " +
            "-fx-max-width: 40px; " +
            "-fx-max-height: 40px;";

    public static final String SHADOW_CIRCLE_BUTTON = CIRCLE_BUTTON +
            "-fx-effect: dropshadow( gaussian , rgba(0,0,0,0.7) , 10,0,0,1 );";

    public static final String ANSWERED = SHADOW_CIRCLE_BUTTON + "-fx-background-color : #80d9bd";

    public static final String NOT_ANSWERED = SHADOW_CIRCLE_BUTTON + "-fx-background-color : #d61c38";

    public static final String NOT_VISITED = SHADOW_CIRCLE_BUTTON + "-fx-background-color : #d4d2cf";

    public static final String MARK_FOR_REVIEW = SHADOW_CIRCLE_BUTTON + "-fx-background-color : #4760b3";

    public static final String ANSWERED_AND_REVIEW = SHADOW_CIRCLE_BUTTON + "-fx-background-color : #da35f0";

    public static final String ACTIVE_QUESTION = CIRCLE_BUTTON + "-fx-background-color: #00ff00";

}
